# WHMCMS-3Pay-v6/v7
WHMCS Content Management System

How do I install the 3pay module?
To install the 3pay payment module, follow the instructions below:

1. Download the WHMCS payment module WHMCMS-3Pay-v6.
2. Unzip the module to a temporary location on your computer.
3. Copy the “modules” folder from the archive to your base “whmcs” folder (using FTP program or similar)
4. This should NOT overwrite any existing files or folders.
5. Login to the WHMCS Administrator console
6. Using the main menu, navigate to Setup ? Payment Gateways
7. Select “3Pay.co” from the “Activate Gateway” drop-down list and click “Activate”
8. Enter the following details under the “3Pay” heading: 
Client ID = <Integration page>
Client Secret = <Integration page>
Convert To For Processing = None
Click “Save Changes”
9. The module is now and ready.
