<?php
function threepay_config() {
    $configarray = array(
     "FriendlyName" => array("Type" => "System", "Value"=>"3Pay.co (Visa, Mastercard, Bkash, Rocket)"),
     "username" => array("FriendlyName" => "Client ID", "Type" => "text", "Size" => "30", "Description" => "Not yet any ID? create one from https://www.3pay.co", ),
     "transmethod" => array("FriendlyName" => "Client Secret", "Type" => "text", "Size" => "100", ),
    );
	return $configarray;
}

function threepay_link($params) {
	$invoiceid = $params['invoiceid'];
	$amount = $params['amount'];
	$firstname = $params['clientdetails']['firstname'];
	$lastname = $params['clientdetails']['lastname'];
	$companyname = $params['companyname'];
	$systemurl = $params['systemurl'];
    $basecurrencyamount = $params['basecurrencyamount'];
	$basecurrency = $params['basecurrency'];
	
	$url = 'https://www.3pay.co/api/request';
	$fields = array(
						'client_id' =>  $params['username'],
						'client_secret' => $params['transmethod'],
						'amount' => $amount,
						'currency' => $params['currency'],
                        'tran_id' => $invoiceid,
						'desc' => $params["description"],
						'payment_type' => '3Pay',
						'cus_name' => $firstname.' '.$lastname,
						'cus_email' => $params['clientdetails']['email'],
						'cus_add1' => $params['clientdetails']['address1'],
						'cus_add2' => $params['clientdetails']['address2'],
						'cus_city' => $params['clientdetails']['city'],
						'cus_state' => $params['clientdetails']['state'],
						'cus_postcode' => $params['clientdetails']['postcode'],
						'cus_country' => $params['clientdetails']['country'],
						'cus_phone' => $params['clientdetails']['phonenumber'],
						'cus_fax' => $params['clientdetails']['phonenumber'],
						'ship_name' => $firstname.' '.$lastname,
						'ship_add1' => $params['clientdetails']['address1'],
						'ship_add2' => $params['clientdetails']['address2'],
						'ship_city' => $params['clientdetails']['city'],
						'ship_state' => $params['clientdetails']['state'],
						'ship_postcode' => $params['clientdetails']['postcode'],
						'ship_country' => $params['clientdetails']['country'],
						'ip' => $_SERVER["SERVER_ADDR"],
						'notify_url' => $params['systemurl'].'modules/gateways/callback/threepay.php',
						'success_url' => $params['systemurl'].'viewinvoice.php?id='.$invoiceid,
						'cancel_url' => $params['systemurl'].'viewinvoice.php?id='.$invoiceid,
						'custom' => '',
						'opt_a' => '',
						'opt_b' => '',
						'opt_c' => '',
						'opt_d' => '',

				);
//echo '<pre>';
//print_r($fields);
//echo '</pre>';
	
$fields_string = '';
foreach($fields as $key=>$value) { 
    $fields_string .= $key.'='.$value.'&'; 
}

rtrim($fields_string, '&');

$ch = curl_init( $url );
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$server_output = curl_exec ($ch);
curl_close ($ch);
	
$htmlOutput = '<a class="btn btn-success" href="'.$server_output.'">Pay with Credit/Debit Card / Bkash /Rocket</a>';
    return $htmlOutput;
}